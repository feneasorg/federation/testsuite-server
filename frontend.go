//
// Federation Testsuite Server
// Copyright (C) 2018 Lukas Matt <lukas@zauberstuhl.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
package main

import (
  "net/http"
  "html/template"
  "fmt"
  "golang.org/x/oauth2"
  "context"
  "strings"
)

func add(a, b int) int {
  return a + b
}

func length(a []interface{}) int {
  return len(a)
}

func render(w http.ResponseWriter, name string, s interface{}) {
  rootTmpl := template.New("").Funcs(template.FuncMap{
    "add": add,
    "len": length,
    "isGitlab": func(serverType ServerType) bool {
      return serverType == GITLAB
    },
    "removeHTTP": func(s string) string {
      return strings.TrimPrefix(strings.TrimPrefix(s, "https://"), "http://")
    },
    "getOrgaFromSlug": func(s string) string {
      sArr := strings.Split(s, "/")
      if len(sArr) > 0 {
        return strings.Join(sArr[:len(sArr)-1], "/")
      }
      return ""
    },
  })

  tmpl, err := rootTmpl.ParseFiles(
    "templates/header.html",
    fmt.Sprintf("templates/%s", name),
    "templates/footer.html",
  ); if err != nil {
    logger.Println(err)
    fmt.Fprintf(w, "Wasn't able to parse the template")
    return
  }

  err = tmpl.ExecuteTemplate(w, name, s)
  if err != nil {
    logger.Println(err)
    fmt.Fprintf(w, "Wasn't able to execute the template")
    return
  }
}

func indexPage(w http.ResponseWriter, r *http.Request) {
  var projects Projects
  err := projects.FindAll()
  if err != nil {
    logger.Println(err)
    render(w, "error.html", "No projects found")
    return
  }

  render(w, "index.html", projects)
}

func resultPage(w http.ResponseWriter, r *http.Request) {
  accessToken := r.URL.Query().Get("accessToken")
  server := r.URL.Query().Get("server")
  repo := r.URL.Query().Get("repo")
  projectName := r.URL.Query().Get("project")

  secret := Secret(16)
  isGithub := accessToken != "" && repo != "" && projectName != ""
  isGitlab := server != "" && isGithub
  isGithub = isGithub && !isGitlab

  if !isGitlab && !isGithub {
    render(w, "error.html", "Missing parameters: accessToken, server, repo or project")
    return
  }

  if isGithub {
    server = "https://github.com"
  }

  project := Project{
    Name: projectName,
    FQDN: server,
    Slug: repo,
    Token: accessToken,
    Secret: secret,
    OptIn: strings.ToUpper(r.URL.Query().Get("optin")) == "ON",
    OptInFlag: r.URL.Query().Get("optinFlag"),
    OptOutFlag: r.URL.Query().Get("optoutFlag"),
  }

  if isGitlab {
    project.Type = GITLAB

    if !devMode {
      err := createGitlabHook(project)
      if err != nil {
        logger.Println(err)
        render(w, "error.html", err.Error())
        return
      }
    }
  } else if isGithub {
    project.Type = GITHUB

    if !devMode {
      err := createGithubHook(project)
      if err != nil {
        logger.Println(err)
        render(w, "error.html", err.Error())
        return
      }
    }
  }

  err := project.CreateOrUpdate()
  if err != nil {
    logger.Println(err)
    render(w, "error.html", "Cannot insert/update the database record")
    return
  }

  render(w, "result.html", project)
}

func authGitlabPage(w http.ResponseWriter, r *http.Request) {
  render(w, "auth.html", struct{Gitlab bool}{Gitlab: true})
}

func authGithubPage(w http.ResponseWriter, r *http.Request) {
  code := r.URL.Query().Get("code")
  if code != "" {
    tok, err := oauth2GithubConfig.Exchange(context.Background(), code)
    if !devMode && err != nil {
      render(w, "error.html", "Invalid token")
    } else {
      var token string = "1234"
      if !devMode {
        token = tok.AccessToken
      }
      render(w, "auth.html", struct{
        Gitlab bool
        Token string
      }{Gitlab: false, Token: token})
    }
  } else {
    url := oauth2GithubConfig.AuthCodeURL("state", oauth2.AccessTypeOffline)
    http.Redirect(w, r, url, http.StatusMovedPermanently)
  }
}

func generatePictures(w http.ResponseWriter, r *http.Request) {
  path := strings.TrimPrefix(r.URL.Path, "/images/stats/")
  pathParts := strings.Split(path, "/")

  if len(pathParts) == 1 {
    if pathParts[0] == "builds.png" {
      buildsPNG(w, r)
      return
    }
  } else if len(pathParts) > 2 {
    slug := strings.Join(pathParts[1:], "/")
    slug = strings.TrimSuffix(slug, ".svg")
    reportsBadge(pathParts[0], slug, w, r)
    return
  }
  http.NotFound(w, r)
}
