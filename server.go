//
// Federation Testsuite Server
// Copyright (C) 2018 Lukas Matt <lukas@zauberstuhl.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
package main

import (
  "flag"
  "net/http"
  "regexp"
)

func init() {
  flag.StringVar(&configDir, "config-dir", "./", "Where is config.conf located?")
}

type route struct {
  pattern *regexp.Regexp
  handler http.Handler
}

type RegexpHandler struct {
  routes []*route
}

func (h *RegexpHandler) Handler(pattern *regexp.Regexp, handler http.Handler) {
  h.routes = append(h.routes, &route{pattern, handler})
}

func (h *RegexpHandler) HandleFunc(pattern *regexp.Regexp, handler func(http.ResponseWriter, *http.Request)) {
  h.routes = append(h.routes, &route{pattern, http.HandlerFunc(handler)})
}

func (h RegexpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
  for _, route := range h.routes {
    if route.pattern.MatchString(r.URL.Path) {
      route.handler.ServeHTTP(w, r)
      return
    }
  }
  // no pattern matched; send 404 response
  http.NotFound(w, r)
}

func main() {
  flag.Parse()
  LoadConfig()
  LoadSchema()
  if !devMode {
    // start build agent
    go BuildAgent()
  }

  handler := RegexpHandler{}
  handler.HandleFunc(regexp.MustCompile(`^/$`), indexPage)
  handler.HandleFunc(regexp.MustCompile(`^/images/stats/`), generatePictures)
  handler.HandleFunc(regexp.MustCompile("^/auth/github"), authGithubPage)
  handler.HandleFunc(regexp.MustCompile("^/auth/gitlab"), authGitlabPage)
  handler.HandleFunc(regexp.MustCompile("^/result"), resultPage)
  handler.HandleFunc(regexp.MustCompile("^/hook"), webhook)

  logMsg := "Running webserver on :8181"
  if devMode {
    logMsg = "[DEV MODE] " + logMsg
  }
  logger.Println(logMsg)
  logger.Println(http.ListenAndServe(":8181", handler))
}
