//
// Federation Testsuite Server
// Copyright (C) 2018 Lukas Matt <lukas@zauberstuhl.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
package main

import (
  "net/http"
  "github.com/wcharczuk/go-chart"
  "github.com/wcharczuk/go-chart/drawing"
  "github.com/wcharczuk/go-chart/util"
  "time"
  "fmt"
  "sort"
)


type state struct {
  Passed, Failed, Canceled float64
}

type sortTime []time.Time

func (t sortTime) Len() int { return len(t) }

func (t sortTime) Swap(i, j int) {
  t[i], t[j] = t[j], t[i]
}

func (t sortTime) Less(i, j int) bool {
  return t[i].Before(t[j])
}

func buildsPNG(w http.ResponseWriter, r *http.Request) {
  db, err := OpenDatabase()
  if err != nil {
    logger.Println(err)
    return
  }
  defer db.Close()

  var builds Builds
  err = db.Find(&builds).Error
  if err != nil {
    logger.Println(err)
    fmt.Fprintf(w, "ups.. something went wrong :(")
    return
  }

  statesPerDay := make(map[time.Time]*state)
  for _, build := range builds {
    year, month, day := build.UpdatedAt.Date()
    date := time.Date(year, month, day, 0, 0, 0, 0, time.Local)
    if _, ok := statesPerDay[date]; !ok {
      statesPerDay[date] = &state{}
    }
    if build.Status == STATUS_SUCCESS {
      statesPerDay[date].Passed += 1
    } else if build.Status == STATUS_FAILED {
      statesPerDay[date].Failed += 1
    } else if build.Status == STATUS_CANCELED {
      statesPerDay[date].Canceled += 1
    }
  }

  // sort
  var keys sortTime
  for key := range statesPerDay {
    keys = append(keys, key)
  }
  sort.Sort(keys)

  // max entries
  if len(statesPerDay) > 5 {
    keys = keys[len(keys)-5:len(keys)]
  }

  var bars []chart.StackedBar
  for _, date := range keys {
    value := statesPerDay[date]
    bars = append(bars, chart.StackedBar{
      Name: date.Format("02-01-2006"),
      Values: []chart.Value{
        {
          Label: "passed",
          Value: value.Passed,
          Style: chart.Style{
            Show: true,
            StrokeWidth: 3,
            StrokeColor: drawing.ColorFromHex("229954"),
            FillColor: drawing.ColorFromHex("27AE60").WithAlpha(64),
          },
        },
        {
          Label: "failed",
          Value: value.Failed,
          Style: chart.Style{
            Show: true,
            StrokeWidth: 3,
            StrokeColor: drawing.ColorFromHex("A93226"),
            FillColor: drawing.ColorFromHex("CD6155").WithAlpha(64),
          },
        },
        {
          Label: "canceled",
          Value: value.Canceled,
          Style: chart.Style{
            Show: true,
            StrokeWidth: 3,
            StrokeColor: drawing.ColorFromHex("666666"),
            FillColor: drawing.ColorFromHex("999999").WithAlpha(64),
          },
        },
      },
    })
  }

  graph := chart.StackedBarChart{
    Title: "Recent Builds",
    TitleStyle: chart.StyleShow(),
    Background: chart.Style{Padding: chart.Box{Top: 40}},
    Height: 512,
    Width: 1024,
    XAxis: chart.StyleShow(),
    YAxis: chart.StyleShow(),
    Bars: bars,
  }
  graph.Elements = []chart.Renderable{Legend(&graph)}

  w.Header().Set("Content-Type", "image/png")
  graph.Render(chart.PNG, w)
}


func Legend(c *chart.StackedBarChart, userDefaults ...chart.Style) chart.Renderable {
  return func(r chart.Renderer, cb chart.Box, chartDefaults chart.Style) {
    legendDefaults := chart.Style{
      FillColor:   drawing.ColorWhite,
      FontColor:   chart.DefaultTextColor,
      FontSize:    8.0,
      StrokeColor: chart.DefaultAxisColor,
      StrokeWidth: chart.DefaultAxisLineWidth,
    }
    var legendStyle chart.Style
    if len(userDefaults) > 0 {
      legendStyle = userDefaults[0].InheritFrom(chartDefaults.InheritFrom(legendDefaults))
    } else {
      legendStyle = chartDefaults.InheritFrom(legendDefaults)
    }

    // DEFAULTS
    legendPadding := chart.Box{
      Top: 10, Left: 10, Right: 10, Bottom: 10}
    lineTextGap := 5
    lineLengthMinimum := 25

    var labels []string
    var lines []chart.Style
    for _, bar := range c.Bars {
      for _, value := range bar.Values {
        exists := false
        for _, label := range labels {
          if label == value.Label {
            exists = true
          }
        }
        if exists {
          continue
        }
        labels = append(labels, value.Label)
        lines = append(lines, value.Style)
      }
    }

    legend := chart.Box{
      Top: cb.Top,
      // XXX 150 should be dynamic not hard-coded :\
      Left: c.GetWidth() - 150,
    }
    legendContent := chart.Box{
      Top: legend.Top + legendPadding.Top,
      Left: legend.Left + legendPadding.Left,
      Right: legend.Left + legendPadding.Left,
      Bottom: legend.Top + legendPadding.Top,
    }
    legendStyle.GetTextOptions().WriteToRenderer(r)

    // measure
    labelCount := 0
    for x := 0; x < len(labels); x++ {
      if len(labels[x]) > 0 {
        tb := r.MeasureText(labels[x])
        if labelCount > 0 {
          legendContent.Bottom += chart.DefaultMinimumTickVerticalSpacing
        }
        legendContent.Bottom += tb.Height()
        right := legendContent.Left + tb.Width() + lineTextGap + lineLengthMinimum
        legendContent.Right = util.Math.MaxInt(legendContent.Right, right)
        labelCount++
      }
    }

    legend = legend.Grow(legendContent)
    legend.Right = legendContent.Right + legendPadding.Right
    legend.Bottom = legendContent.Bottom + legendPadding.Bottom

    chart.Draw.Box(r, legend, legendStyle)

    legendStyle.GetTextOptions().WriteToRenderer(r)

    ycursor := legendContent.Top
    tx := legendContent.Left
    legendCount := 0
    var label string
    for x := 0; x < len(labels); x++ {
      label = labels[x]
      if len(label) > 0 {
        if legendCount > 0 {
          ycursor += chart.DefaultMinimumTickVerticalSpacing
        }

        tb := r.MeasureText(label)
        ty := ycursor + tb.Height()
        r.Text(label, tx, ty)

        th2 := tb.Height() >> 1

        lx := tx + tb.Width() + lineTextGap
        ly := ty - th2
        lx2 := legendContent.Right - legendPadding.Right

        r.SetStrokeColor(lines[x].GetStrokeColor())
        r.SetStrokeWidth(lines[x].GetStrokeWidth())
        r.SetStrokeDashArray(lines[x].GetStrokeDashArray())

        r.MoveTo(lx, ly)
        r.LineTo(lx2, ly)
        r.Stroke()

        ycursor += tb.Height()
        legendCount++
      }
    }
  }
}
