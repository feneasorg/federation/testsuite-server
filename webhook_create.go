//
// Federation Testsuite Server
// Copyright (C) 2018 Lukas Matt <lukas@zauberstuhl.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
package main

import (
  "errors"
  "github.com/google/go-github/v29/github"
  "golang.org/x/oauth2"
  "io/ioutil"
  "net/url"
  "encoding/json"
  "context"
  "strings"
  "fmt"
)

func createGithubHook(project Project) error {
  ctx := context.Background()
  client := github.NewClient(
    oauth2.NewClient(ctx, oauth2.StaticTokenSource(
      &oauth2.Token{AccessToken: project.Token})))

  slug := strings.Split(project.Slug, "/")
  if len(slug) != 2 {
    // github doesn't support subgroups
    return errors.New("Invalid repository slug")
  }

  active := true
  newHook := github.Hook{
    Active: &active,
    Events: []string{"pull_request"},
    Config: map[string]interface{}{
      "content_type": "json",
      "url": fmt.Sprintf("%s/hook", confd.StringDefault(
        "server.domain", "localhost:8181")),
      "secret": project.Secret,
    },
  }

  hooks, _, err := client.Repositories.ListHooks(
    ctx, slug[0], slug[1], &github.ListOptions{})
  if err != nil {
    return err
  }

  var hookExists = false
  hookURL, _ := newHook.Config["url"].(string)
  for _, hook := range hooks {
    curHookURL, _ := hook.Config["url"].(string)
    if hookURL == curHookURL {
      hookExists = true
    }
  }

  if !hookExists {
    _, _, err := client.Repositories.CreateHook(
      ctx, slug[0], slug[1], &newHook)
    if err != nil {
      return err
    }
  }
  return nil
}

func createGitlabHook(project Project) error {
  serverUrl := fmt.Sprintf(
    "%s/hook", confd.StringDefault("server.domain", "http://localhost:8181"))
  path := fmt.Sprintf(
    "/projects/%s/hooks", strings.Replace(project.Slug, "/", "%2F", -1))

  resp, err := project.ApiRequest("GET", path, url.Values{})
  if err != nil {
    return err
  }
  defer resp.Body.Close()

  raw, err := ioutil.ReadAll(resp.Body)
  if err != nil {
    return err
  }

  if resp.StatusCode >= 300 {
    // something went wrong
    return errors.New(string(raw))
  }

  hooks := []struct{URL string `json:"url"`}{}
  err = json.Unmarshal(raw, &hooks)
  if err != nil {
    return err
  }

  for _, hook := range hooks {
    if hook.URL == serverUrl {
      return nil
    }
  }

  values := url.Values{}
  values.Set("url", serverUrl)
  values.Set("merge_requests_events", "1")
  values.Set("push_events", "0")
  values.Set("token", project.Secret)

  resp, err = project.ApiRequest("POST", path, values)
  if err != nil {
    return err
  }
  defer resp.Body.Close()

  raw, err = ioutil.ReadAll(resp.Body)
  if err != nil {
    return err
  }

  if resp.StatusCode >= 300 {
    // something went wrong
    return errors.New(string(raw))
  }
  return nil
}
