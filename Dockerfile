FROM golang:1.13

ENV GI_DIR $GOPATH/src/git.feneas.org/feneas/federation/testsuite-server
RUN mkdir -p $GI_DIR
COPY . $GI_DIR
WORKDIR $GI_DIR

RUN go build -o /tmp/server
RUN mv templates /tmp/templates

FROM debian:stable

RUN useradd -ms /bin/bash user

COPY --from=0 /tmp/server /usr/local/bin/server
COPY --from=0 /tmp/templates /home/user/templates
RUN chown -R user:user /home/user/templates

USER user
WORKDIR /home/user

EXPOSE 8181

ENTRYPOINT ["/usr/local/bin/server"]
