//
// Federation Testsuite Server
// Copyright (C) 2018 Lukas Matt <lukas@zauberstuhl.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
package main

import (
  "time"
  "fmt"
  "strings"
  "io/ioutil"
  "golang.org/x/oauth2"
  "github.com/google/go-github/v29/github"
  "context"
  "net/url"
  "regexp"
  "errors"
  "encoding/json"
)

func (build *Build) Run() {
  if build.PipelineID <= 0 {
    err := build.Pipeline()
    if err != nil {
      logger.Printf("#%d: cannot trigger pipeline: %+v\n", build.ID, err)
      return
    }
    err = build.Update(STATUS_PENDING)
    if err != nil {
      logger.Printf("#%d: cannot update status: %+v\n", build.ID, err)
      return
    }
  }

  started := time.Now()
  timeout := started.Add(-1 * time.Hour)
  for {
    status, err := build.GetStatus()
    logger.Printf("#%d: status=%d error=%+v\n", build.ID, status, err)

    // update status if it differs
    if build.Status != status {
      err = build.Update(status)
      if err != nil {
        logger.Printf("#%d: cannot update status: %+v\n", build.ID, err)
      }
    }

    // stop watching if it failed or succeeded
    if status > STATUS_RUNNING {
      err = build.Report() // fetch build report from remote
      if err != nil {
        logger.Printf("#%d: cannot fetch report: %+v\n", build.ID, err)
      }
      break
    }

    if time.Now().Before(timeout) {
      logger.Printf("#%d: Timeout..\n", build.ID)

      err = build.Update(STATUS_CANCELED)
      if err != nil {
        logger.Printf("#%d: cannot update status: %+v\n", build.ID, err)
      }
      break
    }
    time.Sleep(1 * time.Minute)
  }
  logger.Printf("#%d: pipeline finished\n", build.ID)
}

func (build *Build) Report() error {
  reports, err := build.GetReports()
  if err != nil {
    logger.Println(err)
    return err
  }

  reSkip := regexp.MustCompile(`^ok.*#\sskip`)
  reOk := regexp.MustCompile(`^ok`)
  reNot := regexp.MustCompile(`^not`)
  for _, report := range reports {
    var success, fail, skip uint
    for _, line := range strings.Split(report, "\n") {
      if reSkip.MatchString(line) {
        skip += 1
        continue
      }
      if reOk.MatchString(line) {
        success += 1
        continue
      }
      if reNot.MatchString(line) {
        fail += 1
        continue
      }
    }

    dbReport := Report{
      BuildID: build.ID,
      TestsOkCnt: success,
      TestsFailCnt: fail,
      TestsSkipCnt: skip,
      Raw: report,
    }
    err = dbReport.Save()
    if err != nil {
      return err
    }
  }
  return nil
}

func (build *Build) Pipeline() error {
  values := url.Values{}
  values.Set("PROJECT", build.Project.Name)
  values.Set("PRREPO", build.MergeRequest.URL)
  values.Set("PRSHA", build.MergeRequest.Sha)

  resp, err := test_server_pipeline("master", values)
  if err != nil {
    logger.Printf("#%d: Cannot trigger pipeline: %+v\n", build.ID, err)
    return err
  }
  defer resp.Body.Close()

  raw, err := ioutil.ReadAll(resp.Body)
  if err != nil {
    logger.Printf("#%d: Cannot read pipeline body: %+v\n", build.ID, err)
    return err
  }

  if resp.StatusCode >= 300 {
    // something went wrong
    return errors.New(string(raw))
  }

  pipeline := struct {ID uint `json:"id"`}{}
  err = json.Unmarshal(raw, &pipeline)
  if err != nil {
    logger.Printf("#%d: Cannot unmarshal body: %+v <> %s\n",
      build.ID, err, string(raw))
    return err
  }

  build.PipelineID = pipeline.ID
  err = build.Save()
  if err != nil {
    logger.Printf("#%d: Cannot update database: %+v\n", build.ID, err)
    return err
  }
  return nil
}

func (build *Build) get(method, path string, values url.Values) (raw []byte, err error) {
  resp, err := test_server_request(method, path, values)
  if err != nil {
    logger.Printf("#%d: Cannot fetch build status: %+v\n", build.ID, err)
    return raw, err
  }
  defer resp.Body.Close()

  raw, err = ioutil.ReadAll(resp.Body)
  if err != nil {
    logger.Printf("#%d: Cannot read status body: %+v\n", build.ID, err)
    return raw, err
  }

  if resp.StatusCode >= 300 {
    // something went wrong
    return raw, errors.New(string(raw))
  }
  return raw, nil
}

func (build *Build) GetReports() (reports []string, err error) {
  jobs := []struct {ID int `json:"id"`}{}
  raw, err := build.get("GET", fmt.Sprintf("/projects/%d/pipelines/%d/jobs",
    confd.IntDefault("gitlab.project.id", 102), build.PipelineID), url.Values{})
  if err != nil {
    logger.Printf("#%d: Cannot fetch jobs: %+v\n", build.ID, err)
    return reports, err
  }

  err = json.Unmarshal(raw, &jobs)
  if err != nil {
    logger.Printf("#%d: Cannot unmarshal body: %+v <> %s\n",
      build.ID, err, string(raw))
    return reports, err
  }

  for _, job := range jobs {
    raw, err := build.get("GET", fmt.Sprintf(
      "/projects/%d/jobs/%d/artifacts/report.tap",
      confd.IntDefault("gitlab.project.id", 102), job.ID), url.Values{})
    if err != nil {
      logger.Printf("#%d: Cannot fetch artifacts: %+v\n", build.ID, err)
      continue
    }

    reports = append(reports, string(raw))
  }
  return
}

func (build *Build) GetStatus() (BuildStatus, error) {
  raw, err := build.get("GET", fmt.Sprintf("/projects/%d/pipelines/%d",
    confd.IntDefault("gitlab.project.id", 102), build.PipelineID), url.Values{})
  if err != nil {
    logger.Printf("#%d: Cannot fetch build status: %+v\n", build.ID, err)
    return STATUS_PENDING, err
  }

  status := struct {Status string `json:"status"`}{}
  err = json.Unmarshal(raw, &status)
  if err != nil {
    logger.Printf("#%d: Cannot unmarshal body: %+v <> %s\n",
      build.ID, err, string(raw))
    return STATUS_PENDING, err
  }

  switch status.Status {
  case "running":
    return STATUS_RUNNING, nil
  case "success":
    return STATUS_SUCCESS, nil
  case "failed":
    return STATUS_FAILED, nil
  case "canceled":
    return STATUS_CANCELED, nil
  case "pending":
    fallthrough
  default:
    return STATUS_PENDING, nil
  }
}

func (build *Build) Update(status BuildStatus) error {
  var state = "pending"
  switch status {
  case STATUS_RUNNING:
    if build.Project.Type != GITHUB {
      state = "running"
    }
  case STATUS_SUCCESS:
    state = "success"
  case STATUS_FAILED:
    state = "failed"
    if build.Project.Type == GITHUB {
      state = "error"
    }
  case STATUS_CANCELED:
    state = "canceled"
    if build.Project.Type == GITHUB {
      state = "failure"
    }
  }

  pipelineHref := fmt.Sprintf(
    "%s/feneas/federation/testsuite/pipelines/%d", confd.StringDefault(
      "gitlab.server", "https://git.feneas.org"), build.PipelineID)

  statusName := confd.StringDefault("status.name", "Federation Suite")
  statusDescription := confd.StringDefault("status.description",
    "Continuous integration tests for the federation network")

  switch build.Project.Type {
  case GITHUB:
    client := github.NewClient(oauth2.NewClient(context.Background(),
      oauth2.StaticTokenSource(
        &oauth2.Token{AccessToken: build.Project.Token},
      )))

    repoStatus := github.RepoStatus{
      State: &state,
      Description: &statusDescription,
      Context: &statusName,
      TargetURL: &pipelineHref,
    }
    slug := strings.Split(build.Project.Slug, "/")
    if len(slug) != 2 {
      // github doesn't support subgroups
      fmt.Printf("#%d: Invalid repo slug: %s\n", build.ID, build.Project.Slug)
      return errors.New("Invalid repo slug")
    }

    if _, _, err := client.Repositories.CreateStatus(context.Background(),
    slug[0], slug[1], build.MergeRequest.Sha, &repoStatus); err != nil {
      fmt.Printf("#%d: Cannot update status: %+v\n", build.ID, err)
      return err
    }
  case GITLAB:
    values := url.Values{}
    values.Set("state", state)
    values.Set("name", statusName)
    values.Set("description", statusDescription)
    values.Set("target_url", pipelineHref)
    values.Set("ref", build.MergeRequest.SourceBranch)

    _, err := build.Project.ApiRequest("POST", fmt.Sprintf(
      "/projects/%s/statuses/%s",
      strings.Replace(build.Project.Slug, "/", "%2F", -1),
      build.MergeRequest.Sha), values)

    if err != nil {
      fmt.Printf("#%d: Cannot update status: %+v\n", build.ID, err)
      return err
    }
  }

  build.Status = status
  return build.Save()
}
