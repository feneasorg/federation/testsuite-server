//
// Federation Testsuite Server
// Copyright (C) 2018 Lukas Matt <lukas@zauberstuhl.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
package main

import (
  "net/url"
  "net/http"
  "strings"
  "bytes"
  "fmt"
)

func (project *Project) ApiRequest(method, path string, values url.Values) (*http.Response, error) {
  endpoint := fmt.Sprintf("%s/api/v4%s", project.FQDN, path)

  logger.Printf("http method=%s url=%s values=%+v\n", method, endpoint, values)

  req, err := http.NewRequest(method, endpoint,
    bytes.NewBufferString(values.Encode()))
  if err != nil {
    return nil, err
  }

  req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
  req.Header.Set("Accept", "application/json")
  req.Header.Set("PRIVATE-TOKEN", project.Token)

  client := &http.Client{}
  return client.Do(req)
}

func test_server_request(method, path string, values url.Values) (*http.Response, error) {
  endpoint := fmt.Sprintf("%s%s", confd.StringDefault("gitlab.api",
    "https://git.feneas.org/api/v4"), path)

  logger.Printf("http method=%s url=%s values=%+v\n", method, endpoint, values)

  req, err := http.NewRequest(method, endpoint,
    bytes.NewBufferString(values.Encode()))
  if err != nil {
    return nil, err
  }
  req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
  req.Header.Set("Accept", "application/json")
  req.Header.Set("PRIVATE-TOKEN",
    confd.StringDefault("gitlab.token.repository", ""))

  client := &http.Client{}
  return client.Do(req)
}

func test_server_pipeline(ref string, values url.Values) (*http.Response, error) {
  newValues := url.Values{}
  for key, value := range values {
    newValues.Set(fmt.Sprintf("variables[%s]", key), strings.Join(value, ","))
  }
  newValues.Set("token", confd.StringDefault("gitlab.token.trigger", ""))
  newValues.Set("ref", ref)

  return http.PostForm(fmt.Sprintf("%s/projects/%d/trigger/pipeline",
    confd.StringDefault("gitlab.api", "https://git.feneas.org/api/v4"),
    confd.IntDefault("gitlab.project.id", 102)), newValues)
}
