//
// Federation Testsuite Server
// Copyright (C) 2018 Lukas Matt <lukas@zauberstuhl.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
package main

import (
  "github.com/jinzhu/gorm"
  _ "github.com/jinzhu/gorm/dialects/sqlite"
)

func LoadSchema() {
  db, err := OpenDatabase()
  if err != nil {
    panic(err.Error())
  }
  defer db.Close()

  build := &Build{}
  db.Model(build).AddUniqueIndex("index_builds_on_ids",
    "project_id", "merge_request_id", "pipeline_id")
  db.AutoMigrate(build)

  project := &Project{}
  db.Model(project).AddUniqueIndex(
    "index_projects_on_fqdn_and_slug", "fqdn", "slug")
  db.AutoMigrate(project)

  mergeRequest := &MergeRequest{}
  db.AutoMigrate(mergeRequest)

  report := &Report{}
  db.AutoMigrate(report)
}

// merge request table

type MergeRequest struct {
  gorm.Model
  MRID uint
  SourceBranch string
  Sha, URL string
}

// project table

type Project struct {
  gorm.Model

  Name string

  FQDN, Slug, Secret, Token string
  Type ServerType

  Active bool

  OptIn bool
  OptInFlag string `gorm:"default:'ci'"`
  OptOutFlag string `gorm:"default:'ci skip'"`
}

type Projects []Project

func (projects *Projects) FindAll() error {
  db, err := OpenDatabase()
  if err != nil {
    return err
  }
  defer db.Close()

  return db.Find(projects).Error
}

func (project *Project) CreateOrUpdate() error {
  db, err := OpenDatabase()
  if err != nil {
    return err
  }
  defer db.Close()

  var oldRecord Project
  err = db.Where("name = ? and fqdn = ? and slug = ?",
    project.Name, project.FQDN, project.Slug).Find(&oldRecord).Error
  if err == gorm.ErrRecordNotFound {
    err = db.Create(project).Error
    if err != nil {
      return err
    }
  } else if err == nil {
    // NOTE you have to specify opt_in as extra update field
    // since gorm will not update if bool is false
    // see http://gorm.io/docs/update.html
    project.Secret = "" // set to default then it won't update
    err = db.Model(project).Where("id = ?", oldRecord.ID).
      Update(project).Update("opt_in", project.OptIn).Error
    if err != nil {
      return err
    }
  }
  return err
}

func (project *Project) FindByFQDNAndSlug(fqdn, slug string) error {
  db, err := OpenDatabase()
  if err != nil {
    return err
  }
  defer db.Close()

  return db.Where("fqdn = ? and slug = ?", fqdn, slug).Find(project).Error
}

// build table

type Build struct {
  gorm.Model
  MergeRequestID, ProjectID uint

  PipelineID uint
  Status BuildStatus

  Reports Reports
  Project Project
  MergeRequest MergeRequest
}

type Builds []Build

func (build *Build) AfterFind(db *gorm.DB) error {
  err := db.Model(build).Related(&build.Project).Error
  if err != nil {
    return err
  }

  if err := db.Model(build).Related(&build.Reports).Error; err != nil {
    logger.Printf("Cannot find reports for build=%+v", *build)
  }

  return db.Model(build).Related(&build.MergeRequest).Error
}

func (build *Build) Save() error {
  db, err := OpenDatabase()
  if err != nil {
    return err
  }
  defer db.Close()

  return db.Save(build).Error
}

func (build *Build) FindLastByProjectID(id uint) error {
  db, err := OpenDatabase()
  if err != nil {
    return err
  }
  defer db.Close()

  return db.Where("project_id = ?", id).
    Order("pipeline_id desc").First(build).Error
}

// report table

type Report struct {
  gorm.Model

  BuildID uint
  TestsOkCnt uint
  TestsFailCnt uint
  TestsSkipCnt uint

  Raw string
}

type Reports []Report

func (report *Report) Save() error {
  db, err := OpenDatabase()
  if err != nil {
    return err
  }
  defer db.Close()

  return db.Save(report).Error
}
