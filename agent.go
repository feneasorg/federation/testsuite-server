//
// Federation Testsuite Server
// Copyright (C) 2018 Lukas Matt <lukas@zauberstuhl.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
package main

import "time"

func BuildAgent() {
  logger.Println("Started build agent")

  // if we still have some unfinished jobs start watching them
  runAllJobsBelow(STATUS_SUCCESS)

  for {
    // sleep for a bit before continuing
    time.Sleep(10 * time.Second)

    runAllJobsBelow(STATUS_PENDING)
  }
}

func runAllJobsBelow(status BuildStatus) {
  db, err := OpenDatabase()
  if err != nil {
    panic(err.Error())
  }
  defer db.Close()

  var builds Builds
  err = db.Where("status < ?", status).Find(&builds).Error
  if err != nil {
    //logger.Printf("Cannot fetch new builds: %+v\n", err)
    return
  }

  for _, build := range builds {
    logger.Printf("#%d: starting new build\n", build.ID)
    go build.Run()
  }
}
