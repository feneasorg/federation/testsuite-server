//
// Federation Testsuite Server
// Copyright (C) 2018 Lukas Matt <lukas@zauberstuhl.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
package main

import (
  "log"
  "golang.org/x/oauth2"
  oauth2Github "golang.org/x/oauth2/github"
  "github.com/revel/config"
  gocache "github.com/patrickmn/go-cache"
  "github.com/jinzhu/gorm"
  _ "github.com/jinzhu/gorm/dialects/sqlite"
  "math/rand"
  "time"
  "os"
)

var (
  configDir = "./"
  confd *config.Context

  devMode = false
  cache = gocache.New(5*time.Minute, 10*time.Minute)
  logger = log.New(os.Stdout, "", log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile)
  runes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

  oauth2GithubConfig = &oauth2.Config{
    Scopes: []string{"admin:repo_hook", "repo:status"},
    Endpoint: oauth2Github.Endpoint,
  }
)

type ServerType uint

const (
  GITLAB ServerType = iota
  GITHUB
)

type BuildStatus uint

const (
  // The state of the status. Can be one of the following:
  // pending, running, success, failed, canceled
  STATUS_PENDING BuildStatus = 10 + iota
  STATUS_RUNNING
  STATUS_SUCCESS
  STATUS_FAILED
  STATUS_CANCELED
)

func init() {
  rand.Seed(time.Now().UnixNano())
}

func LoadConfig() {
  var err error
  confd, err = config.LoadContext("config.conf", []string{configDir})
  if err != nil {
    panic(err.Error())
  }

  devMode = confd.BoolDefault("development", false)
  oauth2GithubConfig.ClientID = confd.StringDefault("github.id", "")
  oauth2GithubConfig.ClientSecret = confd.StringDefault("github.secret", "")
}

func OpenDatabase() (*gorm.DB, error) {
  return gorm.Open(
    confd.StringDefault("database.driver", "sqlite3"),
    confd.StringDefault("database.dsn", "./server.db"))
}

func SetCache(key string, val interface{}) {
  cache.Set(key, val, gocache.DefaultExpiration)
}

func Secret(n int) string {
  b := make([]rune, n)
  for i := range b {
    b[i] = runes[rand.Intn(len(runes))]
  }
  return string(b)
}
