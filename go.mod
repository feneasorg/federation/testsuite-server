module git.feneas.org/feneas/federation/testsuite-server

go 1.13

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/google/go-github/v29 v29.0.2
	github.com/jinzhu/gorm v1.9.12
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/revel/config v0.21.0
	github.com/rogpeppe/go-internal v1.5.2 // indirect
	github.com/wcharczuk/go-chart v2.0.1+incompatible
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1 // indirect
	golang.org/x/mod v0.2.0 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/tools v0.0.0-20200207224406-61798d64f025 // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
	honnef.co/go/tools v0.0.1-2019.2.3 // indirect
)
