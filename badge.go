//
// Federation Testsuite Server
// Copyright (C) 2018 Lukas Matt <lukas@zauberstuhl.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
package main

import (
 "bytes"
 "text/template"
 "net/http"
 "fmt"
)

type BadgeColor string
var (
  BadgeBrightGreen BadgeColor = "#44cc11"
  BadgeGreen BadgeColor = "#97ca00"
  BadgeYellow BadgeColor = "#dfb317"
  BadgeOrange BadgeColor = "#fe7d37"
  BadgeRed BadgeColor = "#e05d44"
)

// badgeTemplate was copied from https://github.com/imsky/covbadger
// see https://github.com/imsky/covbadger/blob/master/LICENSE
var badgeTemplate string = `<svg xmlns="http://www.w3.org/2000/svg" width="156" height="20">
    <title>Success/Failed/Skipped</title>
    <desc>Generated with the Federation Testsuite</desc>
    <linearGradient id="smooth" x2="0" y2="100%%">
        <stop offset="0" stop-color="#bbb" stop-opacity=".1" />
        <stop offset="1" stop-opacity=".1" />
    </linearGradient>
    <rect rx="3" width="156" height="20" fill="#555" />
    <rect rx="3" x="100" width="56" height="20" fill="{{.Color}}" />
    <rect x="100" width="4" height="20" fill="{{.Color}}" />
    <rect rx="3" width="156" height="20" fill="url(#smooth)" />
    <g fill="#fff" text-anchor="middle" font-family="DejaVu Sans,Verdana,sans-serif" font-size="11">
        <text x="50" y="15" fill="#010101" fill-opacity=".3">federation tests</text>
        <text x="50" y="14">federation tests</text>
        <text x="128" y="15" fill="#010101" fill-opacity=".3">{{.Desc}}</text>
        <text x="128" y="14">{{.Desc}}</text>
    </g>
</svg>`

func reportsBadge(fqdn, slug string, w http.ResponseWriter, r *http.Request) {
  cacheBadgeKey := fmt.Sprintf("images-stats-%s-%s", fqdn, slug)
  svg, found := cache.Get(cacheBadgeKey); if !found {
    var project Project
    err := project.FindByFQDNAndSlug("https://" + fqdn, slug)
    if err != nil {
      logger.Println(err)
      http.NotFound(w, r)
      return
    }

    var build Build
    err = build.FindLastByProjectID(project.ID)
    if err != nil {
      logger.Println(err)
      http.NotFound(w, r)
      return
    }

    var success, fail, skip uint
    for _, report := range build.Reports {
      success += report.TestsOkCnt
      fail += report.TestsFailCnt
      skip += report.TestsSkipCnt
    }

    var color = BadgeRed
    if fail == 0 {
      if skip == 0 && success == 0 {
        color = BadgeGreen
      } else if skip == 0 {
        color = BadgeBrightGreen
      } else if skip < success {
        color = BadgeYellow
      } else {
        color = BadgeOrange
      }
    }

    var buffer bytes.Buffer
    badgeTemplate, err := template.New("badge").Parse(badgeTemplate)
    if err != nil {
      logger.Println(err)
      http.NotFound(w, r)
      return
    }

    badge := struct {
      Desc string
      Color BadgeColor
    }{
      Desc: fmt.Sprintf("%d/%d/%d", success, fail, skip),
      Color: color,
    }

    err = badgeTemplate.Execute(&buffer, &badge)
    if err != nil {
      logger.Println(err)
      http.NotFound(w, r)
      return
    }

    svg = buffer.String()
    SetCache(cacheBadgeKey, svg)
  }

  svgString, ok := svg.(string); if !ok {
    logger.Printf("Cannot type cast to string svg=%+v\n", svg)
    http.NotFound(w, r)
    return
  }

  w.Header().Set("Content-Type", "image/svg+xml")
  fmt.Fprintf(w, svgString)
}
