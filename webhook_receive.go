//
// Federation Testsuite Server
// Copyright (C) 2018 Lukas Matt <lukas@zauberstuhl.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
package main

import (
  "fmt"
  "net/http"
  "github.com/google/go-github/v29/github"
  "io/ioutil"
  "encoding/json"
  "strings"
  "context"
)

type GitlabHook struct {
  Attrs struct {
    IID uint `json:"iid"`
    Title string `json:"title"`
    State string `json:"state"`
    Description string `json:"description"`
    SrcBranch string `json:"source_branch"`
    Source struct {
      HttpUrl string `json:"http_url"`
    } `json:"source"`
    Target struct {
      HttpUrl string `json:"http_url"`
    } `json:"target"`
    Commit struct {
      Sha string `json:"id"`
      Message string `json:"message"`
    } `json:"last_commit"`
  } `json:"object_attributes"`
  Labels []struct {
    Title string `json:"title"`
    Description string `json:"description"`
  } `json:"labels"`
}

func webhook(w http.ResponseWriter, r *http.Request) {
  if r.Header.Get("X-Gitlab-Event") == "Merge Request Hook" {
    logger.Println("X-Gitlab-Event triggered")
    webhookGitlab(w, r)
  } else if r.Header.Get("X-GitHub-Event") == "pull_request" {
    logger.Println("X-GitHub-Event triggered")
    webhookGithub(w, r)
  } else {
    logger.Printf("Unsupported webhook event header=%+v\n", r.Header)
    fmt.Fprintf(w, `{"error":"unsupported webhook event"}`)
  }
}

func webhookGitlab(w http.ResponseWriter, r *http.Request) {
  db, err := OpenDatabase()
  if err != nil {
    logger.Println(err)
    fmt.Fprintf(w, `{"error":"database error"}`)
    return
  }
  defer db.Close()
  defer r.Body.Close()

  raw, err := ioutil.ReadAll(r.Body)
  if err != nil {
    logger.Println(err)
    fmt.Fprintf(w, `{"error":"invalid body"}`)
    return
  }

  var gitlabHook GitlabHook
  err = json.Unmarshal(raw, &gitlabHook)
  if err != nil {
    logger.Println("Cannot parse payload", err.Error())
    fmt.Fprintf(w, `{"error":"cannot parse payload"}`)
    return
  }

  if gitlabHook.Attrs.State != "opened" {
    logger.Printf(
      "Ignoring merge request with state=%s\n", gitlabHook.Attrs.State)
    fmt.Fprintf(w, `{}`)
    return
  }

  httpUrlParts := strings.Split(gitlabHook.Attrs.Target.HttpUrl, "/")
  if len(httpUrlParts) < 5 {
    logger.Println(
      "Cannot extract fqdn and slug", gitlabHook.Attrs.Target.HttpUrl)
    fmt.Fprintf(w, `{"error":"cannot extract fqdn and slug"}`)
    return
  }
  fqdn := fmt.Sprintf("%s//%s", httpUrlParts[0], httpUrlParts[2])
  httpUrlParts[len(httpUrlParts)-1] = (strings.Split(
    httpUrlParts[len(httpUrlParts)-1], "."))[0]
  slug := strings.Join(httpUrlParts[3:], "/")

  logger.Printf("Request from fqdn=%s slug=%s", fqdn, slug)

  var project Project
  err = db.Where("fqdn = ? and slug = ?", fqdn, slug).Find(&project).Error
  if err != nil {
    logger.Println("Repository not registered!")
    fmt.Fprintf(w, `{"error":"repository not registered"}`)
    return
  }

  // validation
  if r.Header.Get("X-Gitlab-Token") != project.Secret {
    logger.Println("Forbidden! Secret doesn't match.")
    fmt.Fprintf(w, `{"error":"forbidden"}`)
    return
  }

  var labels []string
  for _, label := range gitlabHook.Labels {
    labels = append(labels, fmt.Sprintf(
      "%s %s", label.Title, label.Description))
  }

  if !runBuild(project,
      gitlabHook.Attrs.Title, gitlabHook.Attrs.Description,
      gitlabHook.Attrs.Commit.Message, labels) {
    logger.Println("Skip build due optin/-out flag")
    fmt.Fprintf(w, `{}`)
    return
  }

  mergeRequest := MergeRequest{
    MRID: gitlabHook.Attrs.IID,
    SourceBranch: gitlabHook.Attrs.SrcBranch,
    Sha: gitlabHook.Attrs.Commit.Sha,
    URL: gitlabHook.Attrs.Source.HttpUrl,
  }

  err = db.Create(&mergeRequest).Error
  if err != nil {
    logger.Println(err)
    fmt.Fprintf(w, `{"error":"database error"}`)
    return
  }

  build := Build{ProjectID: project.ID, MergeRequestID: mergeRequest.ID}
  err = db.Create(&build).Error
  if err != nil {
    logger.Println(err)
    fmt.Fprintf(w, `{"error":"database error"}`)
    return
  }
  fmt.Fprintf(w, `{}`)
}

func webhookGithub(w http.ResponseWriter, r *http.Request) {
  db, err := OpenDatabase()
  if err != nil {
    logger.Println(err)
    fmt.Fprintf(w, `{"error":"database error"}`)
    return
  }
  defer db.Close()
  defer r.Body.Close()

  b, err := ioutil.ReadAll(r.Body)
  if err != nil {
    logger.Println(err)
    fmt.Fprintf(w, `{"error":"invalid body"}`)
    return
  }

  var event github.PullRequestReviewEvent
  err = json.Unmarshal(b, &event)
  pr := event.PullRequest
  if err != nil || pr == nil {
    logger.Println("Not supported event type", string(b))
    fmt.Fprintf(w, `{"error":"unsupported event type"}`)
    return
  }

  // skip all events except for open PRs
  if pr.GetState() != "open" {
    logger.Println("Ignore closed pull request")
    fmt.Fprintf(w, `{}`)
    return
  }

  var project Project
  err = db.Where("fqdn = ? and slug = ?", "https://github.com",
    pr.GetBase().GetRepo().GetFullName()).Find(&project).Error
  if err != nil {
    logger.Println(err, pr.GetBase().GetRepo().GetFullName())
    fmt.Fprintf(w, `{"error":"repo not registered"}`)
    return
  }

  secret := []byte(project.Secret)
  sig := r.Header.Get("X-Hub-Signature")
  if err := github.ValidateSignature(sig, b, secret); err != nil {
    logger.Printf("validate payload error=%+v", err)
    fmt.Fprintf(w, `{"error":"forbidden"}`)
    return
  }

  var prTitle, prBody, commit string
  var labels []string
  if pr.Title != nil {
    prTitle = pr.GetTitle()
  }
  if pr.Body != nil {
    prBody = pr.GetBody()
  }
  for _, label := range pr.Labels {
    if label.Name != nil {
      labels = append(labels, label.GetName())
    }
  }
  // fetch last commit message
  client := github.NewClient(nil)
  commits, _, err := client.PullRequests.ListCommits(
    context.Background(),
    pr.GetHead().GetUser().GetLogin(),
    pr.GetHead().GetRepo().GetName(),
    pr.GetNumber(), &github.ListOptions{})

  if err == nil && len(commits) > 0 {
    // check only last commit since older ones are irrelevant
    commit = commits[len(commits)-1].GetCommit().GetMessage()
  }

  if !runBuild(project, prTitle, prBody, commit, labels) {
    logger.Println("Skip build due optin/-out flag")
    fmt.Fprintf(w, `{}`)
    return
  }

  mergeRequest := MergeRequest{
    MRID: uint(pr.GetNumber()),
    SourceBranch: pr.GetHead().GetRef(),
    Sha: pr.GetHead().GetSHA(),
    URL: pr.GetHead().GetRepo().GetCloneURL(),
  }

  err = db.Create(&mergeRequest).Error
  if err != nil {
    logger.Println(err)
    fmt.Fprintf(w, `{"error":"database error"}`)
    return
  }

  build := Build{ProjectID: project.ID, MergeRequestID: mergeRequest.ID}
  err = db.Create(&build).Error
  if err != nil {
    logger.Println(err)
    fmt.Fprintf(w, `{"error":"database error"}`)
    return
  }
  fmt.Fprintf(w, `{}`)
}

func runBuild(project Project, prTitle, prBody, commit string, labels []string) bool {
  var flagExists = false
  var buildFlag = project.OptOutFlag
  if project.OptIn {
    buildFlag = project.OptInFlag
  }

  // check PR title and body for [ci] or [ci skip] flag
  if strings.Contains(prTitle, fmt.Sprintf("[%s]", buildFlag)) {
    flagExists = true
  } else if strings.Contains(prBody, fmt.Sprintf("[%s]", buildFlag)) {
    flagExists = true
  } else {
    // check labels for build flag if we haven't already found it
    for _, label := range labels {
      if strings.Contains(label, buildFlag) {
        flagExists = true
        break
      }
    }

    if !flagExists {
      // last but not least check the commit message for flags
      flagExists = strings.Contains(commit, fmt.Sprintf("[%s]", buildFlag))
    }
  }

  // ignoring pull-request! Repository is set
  // to opt-in and no build flag was found
  if project.OptIn && !flagExists {
    logger.Printf("Ignore optin=%t buildFlag=%s flagExists=%t\n",
      project.OptIn, buildFlag, flagExists)
    return false
  }

  // ignoring pull-request! Repository is set
  // to opt-out and a skip flag was found
  if !project.OptIn && flagExists {
    logger.Printf("Ignore optin=%t buildFlag=%s flagExists=%t\n",
      project.OptIn, buildFlag, flagExists)
    return false
  }
  return true
}
